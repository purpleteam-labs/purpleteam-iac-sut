output "aws_api_gateway_vpc_link_sut_nlb_id" {
  value = aws_api_gateway_vpc_link.sut_nlb.id
  sensitive = false
}
