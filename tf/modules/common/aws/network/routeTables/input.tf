variable "vpc_id" {}
variable "gateway_id" {}
variable "public_and_2nd_mandatory_subnet_for_lb_subnet_ids" { type = list }
variable "aws_region" { type = string }
