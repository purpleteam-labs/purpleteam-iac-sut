variable "ec2_instance_public_keys" {
  description = "SSH public keys for ec2 instances."
  type = map(string)
}
