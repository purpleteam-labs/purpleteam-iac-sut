variable "purpleteamlabs_cloudflare_dns_zone_id" { type = string }
variable "purpleteamlabs_domain_name" { type = string }
variable "invoking_root" { type = string }
variable "purpose" { type = string }

